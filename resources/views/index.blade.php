<!DOCTYPE html>
<html lang="en"> 
    <head>
        <title>Shulesoft Support System</title>
        <?php $root = url('/') . '/public/' ?>

        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Bootstrap Documentation Template For Software Developers">
        <meta name="author" content="Xiaoying Riley at 3rd Wave Media">    
        <link rel="shortcut icon" href="favicon.ico"> 

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">

        <!-- FontAwesome JS-->
        <script defer src="<?= $root ?>assets/fontawesome/js/all.min.js"></script>

        <!-- Theme CSS -->  
        <link id="theme-style" rel="stylesheet" href="<?= $root ?>assets/css/theme.css">
        <style>
            .header-img{
                width: 12%;
                position: absolute;
                bottom: 0;
                left: 7.5%;
                left: calc(20% - 150px);
                opacity: .8;
            }
            .header-img-rigth{
                width: 300px;
                position: absolute;
                bottom: 0;
                right: 0;
                opacity: .8;
            }


        </style>
    </head> 

    <body>    
        <header class="header fixed-top">	    
            <div class="branding docs-branding">
                <div class="container-fluid position-relative py-2">
                    <div class="docs-logo-wrapper">
                        <div class="site-logo"><a class="navbar-brand" href="">
                                <img class="logo-icon mr-2" src="<?= $root ?>assets/images/logo.png" width="46" height="46" alt="logo"> 
                                <span class="logo-text">Support<span class="text-alt"> </span></span></a>
                        </div>    
                    </div><!--//docs-logo-wrapper-->
                    <div class="docs-top-utilities d-flex justify-content-end align-items-center">

                        <ul class=" list-inline mx-md-3 mx-lg-5 mb-0 d-none d-lg-flex">
<!--                            <li class="list-inline-item"><a href="#">Help</a></li>
                            <li class="list-inline-item"><a href="#">FAQ</a></li>
                            <li class="list-inline-item"><a href="#">Forum</a></li>-->

                        </ul><!--//social-list-->
                        <a href="<?= url('Support/show') ?>" class="btn btn-primary d-none d-lg-flex">Get Started</a>
                    </div><!--//docs-top-utilities-->
                </div><!--//container-->
            </div><!--//branding-->
        </header><!--//header-->

        <div class="page-header theme-bg-dark py-5 text-center position-relative" style="background-color: #17B1A4;">
            <div class="theme-bg-shapes-right"></div>
            <div class="theme-bg-shapes-left"></div>
            <div class="container">
                <h1 class="page-heading single-col-max mx-auto">How Can We Help You ?</h1>
                <div class="page-intro m-3 single-col-max mx-auto">Everything you need to know about <b>Shulesoft</b> System.</div>
                <img src="{{url('public/images/hello.svg')}}" class="rounded float-left header-img" alt="...">
                <img src="{{url('public/images/collaborate.svg')}}" class="rounded float-left header-img-rigth" alt="...">
                <div class="main-search-box pt-3 d-block mx-auto">
                    <form class="search-form w-100 pb-3" method="POST" action="Support/show">
                        <input type="text" placeholder="What are you looking For..." name="search" class="form-control search-input">
                        <button type="submit" class="btn search-btn" value="Search"><i class="fas fa-search"></i></button>
                        <?= csrf_field() ?>
                    </form>
                </div>
            </div>
        </div><!--//page-header-->
        <div class="page-content">
            <div class="container">
                <div class="docs-overview py-5">
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-4 py-3">
                            <div class="card shadow-sm">
                                <div class="card-body">
                                    <h5 class="card-title mb-3">
                                        <span class="theme-icon-holder card-icon-holder mr-2">
                                            <i class="fas fa-list"></i>
                                        </span><!--//card-icon-holder-->
                                        <span class="card-title-text text-success"> Help Documentation</span>
                                    </h5>
                                    <div class="card-text">
                                        Get started with Shulesoft and learn about its features using our comprehensive help guide.
                                    </div>
                                    <a class="card-link-mask" href="<?= url('Support/show/') ?>"></a>
                                </div><!--//card-body-->
                            </div><!--//card-->
                        </div><!--//col-->
                        <div class="col-12 col-lg-4 py-3">
                            <div class="card shadow-sm">
                                <div class="card-body">
                                    <h5 class="card-title mb-3">
                                        <span class="theme-icon-holder card-icon-holder mr-2">
                                            <i class="fas fa-lightbulb"></i>
                                        </span><!--//card-icon-holder-->
                                        <span class="card-title-text text-success">Discussion Forums</span>
                                    </h5>
                                    <div class="card-text">
                                        Find detailed answers to the most common questions you might have while using ShuleSoft System
                                    </div>
                                    <a class="card-link-mask" href="https://forum.shulesoft.com?source=support"></a>
                                </div><!--//card-body-->
                            </div><!--//card-->
                        </div><!--//col-->
                        <div class="col-12 col-lg-4 py-3">
                            <div class="card shadow-sm">
                                <div class="card-body">
                                    <h5 class="card-title mb-3">
                                        <span class="theme-icon-holder card-icon-holder mr-2">
                                            <i class="fas fa-video fa-fw"></i>
                                        </span><!--//card-icon-holder-->
                                        <span class="card-title-text  text-success">ShuleSoft Academy</span>
                                    </h5>
                                    <div class="card-text">
                                       Learn Step By Step on how you can use ShuleSoft through highly organized videos
                                    </div>
                                    <a class="card-link-mask" href="https://academy.shulesoft.com?source=support"></a>
                                </div><!--//card-body-->
                            </div><!--//card-->
                        </div><!--//col-->
                        <div class="col-12 col-lg-4 py-3">
                            <div class="card shadow-sm">
                                <div class="card-body">
                                    <h5 class="card-title mb-3">
                                        <span class="theme-icon-holder card-icon-holder mr-2">
                                            <i class="fas fa-question"></i>
                                        </span><!--//card-icon-holder-->
                                        <span class="card-title-text text-success">Shulesoft FAQ's</span>
                                    </h5>
                                    <div class="card-text">
                                    Frequently Asked Questions. Find detailed answers to the most common asked questions
                                    </div>
                                    <a class="card-link-mask" href="https://www.shulesoft.com/faq"></a>
                                </div><!--//card-body-->
                            </div><!--//card-->
                        </div><!--//col-->
                        <div class="col-12 col-lg-4 py-3">
                            <div class="card shadow-sm">
                                <div class="card-body">
                                    <h5 class="card-title mb-3">
                                        <span class="theme-icon-holder card-icon-holder mr-2">
                                            <i class="fas fa-bell"></i>
                                        </span><!--//card-icon-holder-->
                                        <span class="card-title-text text-success">News and Updates</span>
                                    </h5>
                                    <div class="card-text">
                                    ShuleSoft News. See what's happening  about ShuleSoft System
                                    </div>
                                    <a class="card-link-mask" href="https://blog.shulesoft.com?source=support"></a>
                                </div><!--//card-body-->
                            </div><!--//card-->
                        </div><!--//col-->
                        <div class="col-12 col-lg-4 py-3">
                            <div class="card shadow-sm">
                                <div class="card-body">
                                    <h5 class="card-title mb-3">
                                        <span class="theme-icon-holder card-icon-holder mr-2">
                                        <i class="fas fa-phone-alt"></i>                                        </span><!--//card-icon-holder-->
                                        <span class="card-title-text text-success"> Contact us</span>
                                    </h5>
                                    <div class="card-text">
                                            Find out from here, the easiest ways to contact us .
                                    </div>
                                    <a class="card-link-mask" href="https://www.shulesoft.com/contact-us"></a>
                                </div><!--//card-body-->
                            </div><!--//card-->
                        </div><!--//col-->
                    </div><!--//row-->

                </div><!--//container-->
            </div><!--//container-->
        </div><!--//page-content-->





        <footer class="footer  bg-light  mb-0" >
            <div class="container">
                <div class="row align-items-center justify-content-md-between py-4 mt-4 delimiter-top">
                    <div class="col-md-6">
                        <div class="copyright  text-sm font-weight-bold text-center text-md-left">
                            &copy; {{date('Y')}} <a href="http://inetstz.com/" class="font-weight-bold" target="_blank">INETS Co Ltd</a>. All rights reserved.
                        </div>
                    </div>
                    <div class="col-md-6">
                        <ul class="nav justify-content-center justify-content-md-end mt-3 mt-md-0">
                            <li class="nav-item">
                                <a class="nav-link" href="https://twitter.com/shulesoft" target="_blank">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="https://instagram.com/shulesoft_" target="_blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="https://facebook.com/shulesoft" target="_blank">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Javascript -->          
        <script src="<?= $root ?>assets/plugins/jquery-3.4.1.min.js"></script>
        <script src="<?= $root ?>assets/plugins/popper.min.js"></script>
        <script src="<?= $root ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>  

    </body>
</html> 

