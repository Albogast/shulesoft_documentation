<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>Shulesoft System Documentation</title>
    <?php $root = url('/') . '/public/' ?>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Bootstrap Documentation Template For Software Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="<?=$root?>assets/fontawesome/js/all.min.js"></script>
    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="<?=$root?>assets/css/theme.css">

<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>

    <script src="uploadcare.js"></script>
</head> 

<body>    
    <header class="header fixed-top">	    
        <div class="branding docs-branding">
            <div class="container-fluid position-relative py-2">
                <div class="docs-logo-wrapper">
	                <div class="site-logo"><a class="navbar-brand" href="">
					<!-- <img class="logo-icon mr-2" src="<?=$root?>assets/images/coderdocs-logo.svg" alt="logo"> -->
					<span class="logo-text">ShuleSoft<span class="text-alt"> Docs</span></span></a>
					</div>    
                </div><!--//docs-logo-wrapper-->
	            <div class="docs-top-utilities d-flex justify-content-end align-items-center">
	
					<ul class="social-list list-inline mx-md-3 mx-lg-5 mb-0 d-none d-lg-flex">
						<li class="list-inline-item"><a href="#"><i class="fab fa-github fa-fw"></i></a></li>
			            <li class="list-inline-item"><a href="#"><i class="fab fa-twitter fa-fw"></i></a></li>
		                <li class="list-inline-item"><a href="#"><i class="fab fa-slack fa-fw"></i></a></li>
		                <li class="list-inline-item"><a href="#"><i class="fab fa-product-hunt fa-fw"></i></a></li>
		            </ul><!--//social-list-->
		            <a href="<?= url('Support/show')?>" class="btn btn-primary d-none d-lg-flex">Download</a>
	            </div><!--//docs-top-utilities-->
            </div><!--//container-->
        </div><!--//branding-->
    </header><!--//header-->
    
    <div class="page-content">
	    <div class="container">
        <form method="post" action="<?= url('customer/createGuide') ?>">

                    <div class="form-group">
                       <h1>How to <?= ucfirst(str_replace('_', ' ', $guide->name))?>  </h1>
                       <hr>
                    </div>

                
                <div class="form-group">
                    <label for="message-text" class="control-label">Content:</label>
                    <textarea id="summernote"  name="content"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="add_faq">Submit</button>
            </div>
            <?= csrf_field() ?>
        </form>

				
            </div><!--//row-->
		    </div><!--//container-->
		</div><!--//container-->
    </div><!--//page-content-->

  
               
    
    <footer class="footer">

	    <div class="footer-bottom text-center py-5">
		    
		    <ul class="social-list list-unstyled pb-4 mb-0">
			    
	            <li class="list-inline-item"><a href="#"><i class="fab fa-twitter fa-fw"></i></a></li>
	           
	            <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f fa-fw"></i></a></li>
	            <li class="list-inline-item"><a href="#"><i class="fab fa-instagram fa-fw"></i></a></li>
	        </ul><!--//social-list-->
	        
	        <!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
            <small class="copyright">Designed with <i class="fas fa-heart" style="color: #fb866a;"></i> by <a class="theme-link" href="http://themes.3rdwavemedia.com" target="_blank">Xiaoying Riley</a> for developers</small>
            
	        
	    </div>
	    
    </footer>
       
    <!-- Javascript -->          
    <script src="<?=$root?>assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="<?=$root?>assets/plugins/popper.min.js"></script>
    <script src="<?=$root?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>  

</body>
</html> 
                                
<script type="text/javascript">
  $(function() {
    $('#summernote').summernote({
      // unfortunately you can only rewrite
      // all the toolbar contents, on the bright side
      // you can place uploadcare button wherever you want
      toolbar: [
        ['uploadcare', ['uploadcare']], // here, for example
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert', ['media', 'link', 'hr', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview']],
        ['help', ['help']]
      ],
      uploadcare: {
        // button name (default is Uploadcare)
        buttonLabel: 'Image / file',
        // font-awesome icon name (you need to include font awesome on the page)
        buttonIcon: 'picture-o',
        // text which will be shown in button tooltip
        tooltipText: 'Upload files or video or something',

        // uploadcare widget options, see https://uploadcare.com/documentation/widget/#configuration
        publicKey: 'demopublickey', // set your API key
        crop: 'free',
        tabs: 'all',
        multiple: true
      }
    });
  });
</script>
