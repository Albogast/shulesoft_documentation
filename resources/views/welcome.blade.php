<div class="container">
    <article class="docs-article" id="section-1">
        <header class="docs-header">
            <h1 class="docs-heading">Introduction <span class="docs-time">Last updated: <?= date('d M Y', strtotime($last_update)) ?></span></h1>
            <section class="docs-intro">
                <h5>What is ShuleSoft</h5>
                ShuleSoft is an extensible, scalable and easy to use school management system that simplify school operations and interconnect parents, teachers, students and other school’s stakeholders
                <br/><br/>
                <p>This Guide is designed to help you get started and understand easily how to use ShuleSoft</p>
                <div id="gist96475800" class="gist">
                    <img src="<?= url('public/images/system_overview.png') ?>" height="700" width="1031"/>
                </div>


            </section><!--//docs-intro-->

            <!--            <h5>Overview about system arrangement:</h5>
                        <p>You can <a class="theme-link" href="https://gist.github.com/" target="_blank">embed your code snippets using Github gists</a></p>
                        <div class="docs-code-block">-->
            <!-- ** Embed github code starts ** -->



            <!-- ** Embed github code ends ** -->
            </div><!--//docs-code-block-->

            <h5> ShuleSoft Users</h5>
            <p> Anyone with an account withing ShuleSoft, can access specific details in ShuleSoft system based on permission granted by school administrator.</p>

            <p>ShuleSoft main users includes </p>
            <ul>
                <li>Students  </li>
                <li>Teachers</li>
                <li>Parents</li>
                <li> and Non-teaching staff</li>
            </ul>


            <h5> How to access ShuleSoft (Requirements) </h5>
            In order to access ShuleSoft, these are requirements
            <ul>
                As a school
                <li> 1.	At least one computer </li>
                <li> 2.	Electricity </li>
                <li> 3.	Internet (you can use phone internet)</li>
            </ul>

            <div class="callout-block callout-block-info mr-1">
                <div class="content">
                    <h4 class="callout-title">
                        <span class="callout-icon-holder">

                            <i class="fas fa-exclamation-triangle"></i> Other Users
                        </span>

                    </h4>
                    <p> Normal User (parent, teacher, student and non-teaching staff) can access ShuleSoft via
                        Mobile phone/tablet/computer
                        <br/>
                        <a href="https://blog.shulesoft.com/use-android-phone-internet-modem/" target="_blank">Link how to connect smartphone with computer to access internet</a></p>
                </div>
            </div>


        </header>
        <section class="docs-section" id="item-1-1">
            <h5> Overview about system arrangement</h5>
            The image above, shows the basic arrangement of ShuleSoft once sign in. 

            <h5>System arrangement:</h5>
            <div class="table-responsive my-4">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th class="theme-bg-light"><a class="theme-link" href="#">A</a></th>
                            <td> System navigation (menu) </td>
                        </tr>
                        <tr>
                            <th class="theme-bg-light"><a class="theme-link" href="#">B</a></th>
                            <td>Search Area </td>
                        </tr>

                        <tr>
                            <th class="theme-bg-light"><a class="theme-link" href="#">C</a></th>
                            <td>All system notifications (birthdays, e-payments, online admission etc) </td>
                        </tr>

                        <tr>
                            <th class="theme-bg-light"><a class="theme-link" href="#">D</a></th>
                            <td>Language changes </td>
                        </tr>
                        <tr>
                            <th class="theme-bg-light"><a class="theme-link" href="#">E</a></th>
                            <td>Messages/replies from parents</td>
                        </tr>
                        <tr>
                            <th class="theme-bg-light"><a class="theme-link" href="#">F</a></th>
                            <td>profile page link</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section><!--//section-->

        <section class="docs-section" id="item-1-2">
            <h5>   Quick Guide</h5>
            For getting started easily, you just need to follow these 4 major steps. These steps are further described on the left side menu with more details, but you can just follow these steps without detailed description below

            <div class="callout-block callout-block-warning mr-1">
                <div class="content">
                    <h4 class="callout-title">
                        <span class="callout-icon-holder">

                            <i class="fas fa-bullhorn "></i> You MUST first login as Admin to perform any major system configuration
                        </span>

                    </h4>
                </div>
            </div>
            <ol>
                <li>	Setup your account </li>
                <p> a)	Navigate to <b>Settings</b> -> <b>System Setting</b>, then fill proper details in all sub-sections available (general, terms, school logo, academic year)</p>
                <p> b)	Navigate to <b>Settings</b> -> <b>User permission</b>, then create roles available in your school, select a role and provide necessary permissions </p>
                <li>	SMS Configuration</li>
                <p> In ShuleSoft, there are two types of SMS (Quick SMS and Phone SMS) </p>
                <ul>
                    <p><b>Quick SMS</b></p>
                    These are internet message that goes with your school Name 
                    <p><b>Phone SMS</b> </p>
                    These are SMS sent from school ANDROID phone
                    Schools needs to Download our ANDROID APP (karibuSMS) in school phone and connect them with ShuleSoft to enable SMS sending from ShuleSoft to parents/users 
                </ul>
                <li>	Data entry </li>
                <p>The followings are the SIX (6) procedures to follow for data entry</p>
                <ol>
                    <li> <a href="<?= url('support/show/add_teacher') ?>" >Register teachers </a> </li>
                    <li> <a href="<?= url('support/show/add_classes') ?>" >Define Classes available in your school,</a> </li>
                    <li> <a href="<?= url('support/show/add_section') ?>" >Define Sections/streams available in your school </a></li>
                    <li> <a href="<?= url('support/show/add_parent') ?>" >Register Parents,</a> </li>
                    <li> <a href="<?= url('support/show/add_student') ?>" >Register Students,</a> </li>
                    <li> <a href="<?= url('support/show/add_user') ?>" >Register school non-teaching staff </a></li>
                </ol>
                <li>	 Basic configuration</li>
                Navigate to <a href="<?= url('support/show/add_signature') ?>" > signature part</a>, and upload school Stamps, this will appear in students’ academic reports and receipts
            </ol>
        </section><!--//section-->

        <section class="docs-section" id="item-1-3">
            <h3> ACADEMIC MANAGEMENT</h3>
            <p>Follow the following steps to setup and use academic parts in ShuleSoft</p>
            <ul>
                <li>Subjects Management</li>
            ·     Add subject
            Click add or import from NECTA (so as to download the subject from NECTA)
            ·         Class subject
            Add subject to a specific class
            ·         Section subject teachers
            Add section subject teachers so as to allocate specific teacher to a specific class

            <li> Grading</li>
            ·         Grading
            Add grades so as to show the rank of the students when entering the mark.
            ·         Special grade name
            This helps to specify the name of grade in the school, if there are some class/exams having different grades.
            ·         Special grade
            This helps to specify other grades in the school, if there are some class/exams having different grades.
            <li>  Exam Settings</li>
            i.            exams group, this help on setting all the exam settings so as its can be easily to use the part of academic by setting the,
            ii.            school exams, to which you enter the exams name of your school
            iii.            Class allocation helps to assign the above entered exams to a specific class.
            <li> Exam schedule</li>
            This help to prepare the time table of what day will the exam be done
            <li> Marking</li>

            This help to add the marks in the system and there are two different ways of enter the marks in the system
            i.            One by one marks enter
            This help to upload the marks of students one by one by click mark and then select class and academic year then you will click view/add mark
            ii.            Upload mark excel
            This help to upload more than one mark in the system by click mark, upload mark excels, excels templates then download to which there you will fill the marks of the respective class, after that go to Upload marks from excel file then down you will submit
            <li> Exam   Reports</li>
            I.            Single reports
            This helps to show the students reports in general and also you can preview the single reports card of a student.
            II.            Combine reports
            This helps to combine more than one exam report and display in general and also you can preview the report card of one student.
            <li> Students character</li>
            Help class teacher and headmaster to write comment on each student’s behavior
            </ul>
        </section>

        <section class="docs-section">

            <h3>  ACCOUNTS AND FINANCE</h3>
            <p>To get started on using accounting module, kindly follow the following simple steps</p>
            <ul>
                <li>Account Settings</li>
            i) banking setup
            ii) Account groups
            iii)charts of accounts 
            iv) Opening balance
            v)Invoice guide
            <li>  Fees Management</li>
            Installments This clarify payments terms of the school by setting the starting date and end date
            Definition This define all the fees of the school and also you will have to allocate bank too 
            Fee structure This help to specify the exactly amount of the fee added before
            Discount This help to show deduction of different fees of a specific students
            Unsubacribe This help to remove a certain fee that a students is not suppose to be paid 
            <li> Invoice Management</li>												
            is a commercial document that itemizes a transaction between a school and a parents to which guide a parents on his/her students bill. 
            <li> Account Transactions</li>
            Include expense,revenue,liability,capital,current and fixed assets
            
            </ul>
        </section>
    

     <section class="docs-section">
        <h3>  Intermediate Guide </h3>
        <p>This basically cover other modules as follows</p>
        Academic (Exams) Management -Intermediate
        Syllabus Management
        Qualitative Reports (Students Characters)

        Library Management
        Configuration (members, books and
        Issuing
        Reports
        Hostel Management
        Manage your hostels
        Transport Management
        Vehicle and Routes
        Members management
        Student Diary
        Attendance
        E-resources
        Routine
        Promotion
        Accounts
        F) Inventory 
        Vendo, Items,Purchase,Usage, System,Pocket money
        G) Payroll
        Pension fund
        is any plan, fund, or scheme which provides retirement income
        Allowance 	
        the act of allowing. an amount or share allotted or granted. a sum of money allotted     or granted for a particular purpose.
        Deduction	
        is making an inference based on widely accepted facts or premises
        Salary 
        This is the payroll which cover the computation for payroll
  
     </section>

     <section class="docs-section">
        <h3>  Advanced Level Guide </h3>
        <p>Mastering these parts usually put your school in the advantage position. Here you need to master</p>
        Academic (Exams) Management - advanced
        Insights academic
        Leaving certificates
        Teachers evaluation
        Accounts Management - advanced
        Loans Management
        Liabilities Management
        Assets Management
        Capital Management
        Financial statement
        Financial ratio
        Financial insights
        Electronic payment integrations
        Users Management
        ID cards
        Contracts Management
        User insights

     </section>
    </article><!--//docs-article-->

    <article class="docs-article" id="section-4">
        <!--        <header class="docs-header">
                    <h1 class="docs-heading">Intergrations</h1>
                    <section class="docs-intro">
                        <p>Section intro goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque finibus condimentum nisl id vulputate. Praesent aliquet varius eros interdum suscipit. Donec eu purus sed nibh convallis bibendum quis vitae turpis. Duis vestibulum diam lorem, vitae dapibus nibh facilisis a. Fusce in malesuada odio.</p>
                    </section>//docs-intro
                </header>
                <section class="docs-section" id="item-4-1">
                    <h2 class="section-heading">Section Item 4.1</h2>
                    <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
                </section>//section
        
                <section class="docs-section" id="item-4-2">
                    <h2 class="section-heading">Section Item 4.2</h2>
                    <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
                </section>//section
        
                <section class="docs-section" id="item-4-3">
                    <h2 class="section-heading">Section Item 4.3</h2>
                    <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
                </section>-->

        <!--//section-->
    </article><!--//docs-article-->
    <!--
        <article class="docs-article" id="section-5">
            <header class="docs-header">
                <h1 class="docs-heading">Utilities</h1>
                <section class="docs-intro">
                    <p>Section intro goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque finibus condimentum nisl id vulputate. Praesent aliquet varius eros interdum suscipit. Donec eu purus sed nibh convallis bibendum quis vitae turpis. Duis vestibulum diam lorem, vitae dapibus nibh facilisis a. Fusce in malesuada odio.</p>
                </section>//docs-intro
            </header>
            <section class="docs-section" id="item-5-1">
                <h2 class="section-heading">Section Item 5.1</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
    
            <section class="docs-section" id="item-5-2">
                <h2 class="section-heading">Section Item 5.2</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
    
            <section class="docs-section" id="item-5-3">
                <h2 class="section-heading">Section Item 5.3</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
        </article>//docs-article
    
    
        <article class="docs-article" id="section-6">
            <header class="docs-header">
                <h1 class="docs-heading">Web</h1>
                <section class="docs-intro">
                    <p>Section intro goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque finibus condimentum nisl id vulputate. Praesent aliquet varius eros interdum suscipit. Donec eu purus sed nibh convallis bibendum quis vitae turpis. Duis vestibulum diam lorem, vitae dapibus nibh facilisis a. Fusce in malesuada odio.</p>
                </section>//docs-intro
            </header>
            <section class="docs-section" id="item-6-1">
                <h2 class="section-heading">Section Item 6.1</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
    
            <section class="docs-section" id="item-6-2">
                <h2 class="section-heading">Section Item 6.2</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
    
            <section class="docs-section" id="item-6-3">
                <h2 class="section-heading">Section Item 6.3</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
        </article>//docs-article
    
    
        <article class="docs-article" id="section-7">
            <header class="docs-header">
                <h1 class="docs-heading">Mobile</h1>
                <section class="docs-intro">
                    <p>Section intro goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque finibus condimentum nisl id vulputate. Praesent aliquet varius eros interdum suscipit. Donec eu purus sed nibh convallis bibendum quis vitae turpis. Duis vestibulum diam lorem, vitae dapibus nibh facilisis a. Fusce in malesuada odio.</p>
                </section>//docs-intro
            </header>
            <section class="docs-section" id="item-7-1">
                <h2 class="section-heading">Section Item 7.1</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
    
            <section class="docs-section" id="item-7-2">
                <h2 class="section-heading">Section Item 7.2</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
    
            <section class="docs-section" id="item-7-3">
                <h2 class="section-heading">Section Item 7.3</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
        </article>//docs-article
    
    
        <article class="docs-article" id="section-8">
            <header class="docs-header">
                <h1 class="docs-heading">Resources</h1>
                <section class="docs-intro">
                    <p>Section intro goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque finibus condimentum nisl id vulputate. Praesent aliquet varius eros interdum suscipit. Donec eu purus sed nibh convallis bibendum quis vitae turpis. Duis vestibulum diam lorem, vitae dapibus nibh facilisis a. Fusce in malesuada odio.</p>
                </section>//docs-intro
            </header>
            <section class="docs-section" id="item-8-1">
                <h2 class="section-heading">Section Item 8.1</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
    
            <section class="docs-section" id="item-8-2">
                <h2 class="section-heading">Section Item 8.2</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
    
            <section class="docs-section" id="item-8-3">
                <h2 class="section-heading">Section Item 8.3</h2>
                <p>Vivamus efficitur fringilla ullamcorper. Cras condimentum condimentum mauris, vitae facilisis leo. Aliquam sagittis purus nisi, at commodo augue convallis id. Sed interdum turpis quis felis bibendum imperdiet. Mauris pellentesque urna eu leo gravida iaculis. In fringilla odio in felis ultricies porttitor. Donec at purus libero. Vestibulum libero orci, commodo nec arcu sit amet, commodo sollicitudin est. Vestibulum ultricies malesuada tempor.</p>
            </section>//section
        </article>//docs-article
    
    
        <article class="docs-article" id="section-9">
            <header class="docs-header">
                <h1 class="docs-heading">FAQs</h1>
                <section class="docs-intro">
                    <p>Section intro goes here. You can list all your FAQs using the format below.</p>
                </section>//docs-intro
            </header>
            <section class="docs-section" id="item-9-1">
                <h2 class="section-heading">Section Item 9.1 <small>(FAQ Category One)</small></h2>
                <h5 class="pt-3"><svg class="svg-inline--fa fa-question-circle fa-w-16 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg> <i class="fas fa-question-circle mr-1"></i> Font Awesome fontawesome.com What's sit amet quam eget lacinia?</h5>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.</p>
                <h5 class="pt-3"><svg class="svg-inline--fa fa-question-circle fa-w-16 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg> <i class="fas fa-question-circle mr-1"></i> Font Awesome fontawesome.com How to ipsum dolor sit amet quam tortor?</h5>
                <p>Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
                <h5 class="pt-3"><svg class="svg-inline--fa fa-question-circle fa-w-16 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg> <i class="fas fa-question-circle mr-1"></i> Font Awesome fontawesome.com Can I  bibendum sodales?</h5>
                <p>Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
                <h5 class="pt-3"><svg class="svg-inline--fa fa-question-circle fa-w-16 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg> <i class="fas fa-question-circle mr-1"></i> Font Awesome fontawesome.com Where arcu sed urna gravida?</h5>
                <p>Aenean et sodales nisi, vel efficitur sapien. Quisque molestie diam libero, et elementum diam mollis ac. In dignissim aliquam est eget ullamcorper. Sed id sodales tortor, eu finibus leo. Vivamus dapibus sollicitudin justo vel fermentum. Curabitur nec arcu sed urna gravida lobortis. Donec lectus est, imperdiet eu viverra viverra, ultricies nec urna. </p>
            </section>//section
    
            <section class="docs-section" id="item-9-2">
                <h2 class="section-heading">Section Item 9.2 <small>(FAQ Category Two)</small></h2>
                <h5 class="pt-3"><svg class="svg-inline--fa fa-question-circle fa-w-16 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg> <i class="fas fa-question-circle mr-1"></i> Font Awesome fontawesome.com What's sit amet quam eget lacinia?</h5>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.</p>
                <h5 class="pt-3"><svg class="svg-inline--fa fa-question-circle fa-w-16 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg> <i class="fas fa-question-circle mr-1"></i> Font Awesome fontawesome.com How to ipsum dolor sit amet quam tortor?</h5>
                <p>Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
                <h5 class="pt-3"><svg class="svg-inline--fa fa-question-circle fa-w-16 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg> <i class="fas fa-question-circle mr-1"></i> Font Awesome fontawesome.com Can I  bibendum sodales?</h5>
                <p>Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
                <h5 class="pt-3"><svg class="svg-inline--fa fa-question-circle fa-w-16 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg> <i class="fas fa-question-circle mr-1"></i> Font Awesome fontawesome.com Where arcu sed urna gravida?</h5>
                <p>Aenean et sodales nisi, vel efficitur sapien. Quisque molestie diam libero, et elementum diam mollis ac. In dignissim aliquam est eget ullamcorper. Sed id sodales tortor, eu finibus leo. Vivamus dapibus sollicitudin justo vel fermentum. Curabitur nec arcu sed urna gravida lobortis. Donec lectus est, imperdiet eu viverra viverra, ultricies nec urna. </p>
            </section>//section
    
            <section class="docs-section" id="item-9-3">
                <h2 class="section-heading">Section Item 9.3 <small>(FAQ Category Three)</small></h2>
            <h5 class="pt-3"><svg class="svg-inline--fa fa-question-circle fa-w-16 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg> <i class="fas fa-question-circle mr-1"></i> Font Awesome fontawesome.com How to dapibus sollicitudin justo vel fermentum?</h5>
                <p>Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
                <h5 class="pt-3"><svg class="svg-inline--fa fa-question-circle fa-w-16 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg> <i class="fas fa-question-circle mr-1"></i> Font Awesome fontawesome.com How long bibendum sodales?</h5>
                <p>Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. </p>
                <h5 class="pt-3"><svg class="svg-inline--fa fa-question-circle fa-w-16 mr-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="question-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M504 256c0 136.997-111.043 248-248 248S8 392.997 8 256C8 119.083 119.043 8 256 8s248 111.083 248 248zM262.655 90c-54.497 0-89.255 22.957-116.549 63.758-3.536 5.286-2.353 12.415 2.715 16.258l34.699 26.31c5.205 3.947 12.621 3.008 16.665-2.122 17.864-22.658 30.113-35.797 57.303-35.797 20.429 0 45.698 13.148 45.698 32.958 0 14.976-12.363 22.667-32.534 33.976C247.128 238.528 216 254.941 216 296v4c0 6.627 5.373 12 12 12h56c6.627 0 12-5.373 12-12v-1.333c0-28.462 83.186-29.647 83.186-106.667 0-58.002-60.165-102-116.531-102zM256 338c-25.365 0-46 20.635-46 46 0 25.364 20.635 46 46 46s46-20.636 46-46c0-25.365-20.635-46-46-46z"></path></svg> <i class="fas fa-question-circle mr-1"></i> Font Awesome fontawesome.com Where dapibus sollicitudin?</h5>
                <p>Aenean et sodales nisi, vel efficitur sapien. Quisque molestie diam libero, et elementum diam mollis ac. In dignissim aliquam est eget ullamcorper. Sed id sodales tortor, eu finibus leo. Vivamus dapibus sollicitudin justo vel fermentum. Curabitur nec arcu sed urna gravida lobortis. Donec lectus est, imperdiet eu viverra viverra, ultricies nec urna. </p>
            </section>//section
        </article>//docs-article
    
        <footer class="footer">
            <div class="container text-center py-5">
                /* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */
            <small class="copyright">Designed with <svg class="svg-inline--fa fa-heart fa-w-16" style="color: #fb866a;" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="heart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z"></path></svg> <i class="fas fa-heart" style="color: #fb866a;"></i> Font Awesome fontawesome.com  by <a class="theme-link" href="http://themes.3rdwavemedia.com" target="_blank">Xiaoying Riley</a> for developers</small>
                <ul class="social-list list-unstyled pt-4 mb-0">
                            <li class="list-inline-item"><a href="#"><svg class="svg-inline--fa fa-github fa-w-16 fa-fw" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="github" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512" data-fa-i2svg=""><path fill="currentColor" d="M165.9 397.4c0 2-2.3 3.6-5.2 3.6-3.3.3-5.6-1.3-5.6-3.6 0-2 2.3-3.6 5.2-3.6 3-.3 5.6 1.3 5.6 3.6zm-31.1-4.5c-.7 2 1.3 4.3 4.3 4.9 2.6 1 5.6 0 6.2-2s-1.3-4.3-4.3-5.2c-2.6-.7-5.5.3-6.2 2.3zm44.2-1.7c-2.9.7-4.9 2.6-4.6 4.9.3 2 2.9 3.3 5.9 2.6 2.9-.7 4.9-2.6 4.6-4.6-.3-1.9-3-3.2-5.9-2.9zM244.8 8C106.1 8 0 113.3 0 252c0 110.9 69.8 205.8 169.5 239.2 12.8 2.3 17.3-5.6 17.3-12.1 0-6.2-.3-40.4-.3-61.4 0 0-70 15-84.7-29.8 0 0-11.4-29.1-27.8-36.6 0 0-22.9-15.7 1.6-15.4 0 0 24.9 2 38.6 25.8 21.9 38.6 58.6 27.5 72.9 20.9 2.3-16 8.8-27.1 16-33.7-55.9-6.2-112.3-14.3-112.3-110.5 0-27.5 7.6-41.3 23.6-58.9-2.6-6.5-11.1-33.3 2.6-67.9 20.9-6.5 69 27 69 27 20-5.6 41.5-8.5 62.8-8.5s42.8 2.9 62.8 8.5c0 0 48.1-33.6 69-27 13.7 34.7 5.2 61.4 2.6 67.9 16 17.7 25.8 31.5 25.8 58.9 0 96.5-58.9 104.2-114.8 110.5 9.2 7.9 17 22.9 17 46.4 0 33.7-.3 75.4-.3 83.6 0 6.5 4.6 14.4 17.3 12.1C428.2 457.8 496 362.9 496 252 496 113.3 383.5 8 244.8 8zM97.2 352.9c-1.3 1-1 3.3.7 5.2 1.6 1.6 3.9 2.3 5.2 1 1.3-1 1-3.3-.7-5.2-1.6-1.6-3.9-2.3-5.2-1zm-10.8-8.1c-.7 1.3.3 2.9 2.3 3.9 1.6 1 3.6.7 4.3-.7.7-1.3-.3-2.9-2.3-3.9-2-.6-3.6-.3-4.3.7zm32.4 35.6c-1.6 1.3-1 4.3 1.3 6.2 2.3 2.3 5.2 2.6 6.5 1 1.3-1.3.7-4.3-1.3-6.2-2.2-2.3-5.2-2.6-6.5-1zm-11.4-14.7c-1.6 1-1.6 3.6 0 5.9 1.6 2.3 4.3 3.3 5.6 2.3 1.6-1.3 1.6-3.9 0-6.2-1.4-2.3-4-3.3-5.6-2z"></path></svg> <i class="fab fa-github fa-fw"></i> Font Awesome fontawesome.com </a></li> 
                    <li class="list-inline-item"><a href="#"><svg class="svg-inline--fa fa-twitter fa-w-16 fa-fw" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="twitter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path></svg> <i class="fab fa-twitter fa-fw"></i> Font Awesome fontawesome.com </a></li>
                    <li class="list-inline-item"><a href="#"><svg class="svg-inline--fa fa-slack fa-w-14 fa-fw" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="slack" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M94.12 315.1c0 25.9-21.16 47.06-47.06 47.06S0 341 0 315.1c0-25.9 21.16-47.06 47.06-47.06h47.06v47.06zm23.72 0c0-25.9 21.16-47.06 47.06-47.06s47.06 21.16 47.06 47.06v117.84c0 25.9-21.16 47.06-47.06 47.06s-47.06-21.16-47.06-47.06V315.1zm47.06-188.98c-25.9 0-47.06-21.16-47.06-47.06S139 32 164.9 32s47.06 21.16 47.06 47.06v47.06H164.9zm0 23.72c25.9 0 47.06 21.16 47.06 47.06s-21.16 47.06-47.06 47.06H47.06C21.16 243.96 0 222.8 0 196.9s21.16-47.06 47.06-47.06H164.9zm188.98 47.06c0-25.9 21.16-47.06 47.06-47.06 25.9 0 47.06 21.16 47.06 47.06s-21.16 47.06-47.06 47.06h-47.06V196.9zm-23.72 0c0 25.9-21.16 47.06-47.06 47.06-25.9 0-47.06-21.16-47.06-47.06V79.06c0-25.9 21.16-47.06 47.06-47.06 25.9 0 47.06 21.16 47.06 47.06V196.9zM283.1 385.88c25.9 0 47.06 21.16 47.06 47.06 0 25.9-21.16 47.06-47.06 47.06-25.9 0-47.06-21.16-47.06-47.06v-47.06h47.06zm0-23.72c-25.9 0-47.06-21.16-47.06-47.06 0-25.9 21.16-47.06 47.06-47.06h117.84c25.9 0 47.06 21.16 47.06 47.06 0 25.9-21.16 47.06-47.06 47.06H283.1z"></path></svg> <i class="fab fa-slack fa-fw"></i> Font Awesome fontawesome.com </a></li>
                    <li class="list-inline-item"><a href="#"><svg class="svg-inline--fa fa-product-hunt fa-w-16 fa-fw" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="product-hunt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M326.3 218.8c0 20.5-16.7 37.2-37.2 37.2h-70.3v-74.4h70.3c20.5 0 37.2 16.7 37.2 37.2zM504 256c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-128.1-37.2c0-47.9-38.9-86.8-86.8-86.8H169.2v248h49.6v-74.4h70.3c47.9 0 86.8-38.9 86.8-86.8z"></path></svg> <i class="fab fa-product-hunt fa-fw"></i> Font Awesome fontawesome.com </a></li>
                    <li class="list-inline-item"><a href="#"><svg class="svg-inline--fa fa-facebook-f fa-w-10 fa-fw" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-f" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg=""><path fill="currentColor" d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path></svg> <i class="fab fa-facebook-f fa-fw"></i> Font Awesome fontawesome.com </a></li>
                    <li class="list-inline-item"><a href="#"><svg class="svg-inline--fa fa-instagram fa-w-14 fa-fw" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="instagram" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path></svg> <i class="fab fa-instagram fa-fw"></i> Font Awesome fontawesome.com </a></li>
                </ul>//social-list
            </div>
        </footer>-->
</div>