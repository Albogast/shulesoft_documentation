<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title><?= ucfirst(str_replace('_', ' ', $guide->permission->name))?></title>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>
    <script src="./uploadcare.js"></script>

    <?php $root = url('/') . '/public/' ?>

<script type="text/javascript" src="<?php echo url('public/assets/select2/select2.js'); ?>"></script>
  
  <!-- Style.css -->

  <link rel="stylesheet" href="<?= $root ?>assets/select2/css/select2.css">

  <link rel="stylesheet" href="<?= $root ?>assets/select2/css/select2-bootstrap.css">

  </head>
  <body>
  <div class="container">

  <form method="post" action="">

<div class="form-group">
   <h1>Edit <?= ucfirst(str_replace('_', ' ', $guide->permission->name))?>  </h1>
   <hr>
</div>

<div class="form-group">
<label for="message-text" class="control-label">Change Group:</label>
<select class="form-control select2" name="permission_id">
<option value="<?=$guide->permission_id?>"><?= ucfirst(str_replace('_', ' ', $guide->permission->name))?></option>
    <?php foreach($permissions as $permission){ ?>
      <option value="<?=$permission->id?>"><?= ucfirst(str_replace('_', ' ', $permission->name))?></option>
    <?php } ?>
    </select>
</div>
<div class="form-group">
<input text="hidden" name="updated_at" value="<?=date('Y-m-d H:i:s')?>">
<textarea id="summernote" name="content" ><?=$guide->content?></textarea>
</div>

<div class="modal-footer">
<a href="<?php echo url('Support/show'); ?>" class="btn btn-default" data-dismiss="modal">Go Back</a>
<button type="submit" class="btn btn-primary" id="add_faq">Submit</button>
</div>
<?= csrf_field() ?>
</form>
</div>

<script>

$(".select2").select2({
    theme: "bootstrap",
    dropdownAutoWidth: false,
    allowClear: false,
    debug: true
});
</script>

<script>
  $(document).ready(function() {
    $('#summernote').summernote({
      height: 460,   //set editable area's height
      // placeholder: "Write User Guide on How to <?= ucfirst(str_replace('_', ' ', $guide->permission->name))?> Here...",
      uploadcare: {
        publicKey: 'demopublickey',
        crop: 'free',
        tabs: 'file url instagram',
        tooltipText: 'Upload files or video or something',
        multiple: true
      },
      toolbar: [
        ['uploadcare', ['uploadcare']], // this is the button
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert', ['media', 'link', 'hr', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview']],
        ['help', ['help']]
      ]
    })
  })
</script>
  </body>
</html>
