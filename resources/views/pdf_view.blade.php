<!DOCTYPE html>
<html lang="en"> 
<head>
<title>Shulesoft System Documentation</title>
	<!-- <?php $root = url('/') . '/public/' ?> -->

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="description" content="Bootstrap 4 Template For Software Startups">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="<?=$root?>assets/fontawesome/js/all.min.js"></script>
    
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.2/styles/atom-one-dark.min.css">

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="<?=$root?>assets/css/theme.css">

</head> 
<style>  
 .main_table{
     padding: 0 10px;
     margin-top:15px;
 }
 .tag{
     width:85.60mm ! important;
     min-height:53.49mm !important;
     height:53.98mm !important;
     page-break-before:auto !important;
     page-break-after:auto !important;
     page-break-inside:avoid !important;
     margin:0 !important;
 }
 @media print{
     pre,blockquote{page-break-inside:avoid;}
 }
 @media print{
     @page{
         margin:1.2cm 1.5cm 1cm 1.5cm;
     }
 }

    </style>

<body>    
		    <div class="main_table">
                

					<?php
					if(!empty($content)){ ?>
					    <h1>How to <?= str_replace('_', ' ', $content->permission->name)?></h1>
					    <div class="tag">
						<p style="page-break-before:auto !important; page-break-after:auto !important; page-breake-inside:avoid !important;"><?=$content->content?></p>
                        </div><!--//docs-intro-->
                    <?php } ?>
              
				    </div> 
</body>

    <!-- Javascript -->          
    <script src="<?=$root?>assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="<?=$root?>assets/plugins/popper.min.js"></script>
    <script src="<?=$root?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>   
    <!-- Page Specific JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/highlight.min.js"></script>
    <script src="<?=$root?>assets/js/highlight-custom.js"></script> 
    <script src="<?=$root?>assets/plugins/jquery.scrollTo.min.js"></script>
    <script src="<?=$root?>assets/plugins/lightbox/dist/ekko-lightbox.min.js"></script> 
    <script src="<?=$root?>assets/js/docs.js"></script> 

</html>