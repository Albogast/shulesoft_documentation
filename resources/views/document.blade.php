<!DOCTYPE html>
<html lang="en"> 
    <head>
        <title>Shulesoft System Documentation</title>
        <?php $root = url('/') . '/public/' ?>

        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
        <meta name="description" content="Bootstrap 4 Template For Software Startups">
        <meta name="author" content="Xiaoying Riley at 3rd Wave Media">    
        <link rel="shortcut icon" href="favicon.ico"> 

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">

        <!-- FontAwesome JS-->
        <script defer src="<?= $root ?>assets/fontawesome/js/all.min.js"></script>

        <!-- Plugins CSS -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.2/styles/atom-one-dark.min.css">

        <!-- Theme CSS -->  
        <link id="theme-style" rel="stylesheet" href="<?= $root ?>assets/css/theme.css">

    </head> 

    <body style="padding:0;margin:0;" class="docs-page">    
        <header class="header fixed-top">	    
            <div class="branding docs-branding">
                <div class="container-fluid position-relative py-2">
                    <div class="docs-logo-wrapper">
                        <button id="docs-sidebar-toggler" class="docs-sidebar-toggler docs-sidebar-visible mr-2 d-xl-none" type="button">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <div class="site-logo"><a class="navbar-brand" href="<?= url('/') ?>">
                                <img class="logo-icon mr-2" src="<?= $root ?>assets/images/logo.png" alt="logo"  width="46" height="46" > 
                                <span class="logo-text"><span class="text-alt"> Documentation</span></span></a>
                        </div>    
                    </div><!--//docs-logo-wrapper-->
                    <div class="docs-top-utilities d-flex justify-content-end align-items-center">
                        <div class="top-search-box d-none d-lg-flex">
                            <form class="search-form" action="" method="POST">
                                <input type="text" placeholder="Search Here..." name="search" class="form-control search-input">
                                <button type="submit" class="btn search-btn" value="Search"><i class="fas fa-search"></i></button>
                                <?= csrf_field() ?>
                            </form>
                        </div>

                        <ul class="social-list list-inline mx-md-3 mx-lg-5 mb-0 d-none d-lg-flex">
                            <li class="list-inline-item"><a href="https://wa.me/255655406004"><i class="fab fa-whatsapp fa-fw"></i></a></li>
                            <li class="list-inline-item"><a href="tel:+255655406004"><i class="fa fa-phone fa-fw"></i></a></li>
                            <li class="list-inline-item"><a href="mailto:support@shulesoft.com"><i class="fa fa-envelope fa-fw"></i></a></li>
                            <!-- <li class="list-inline-item"><a href="#"><i class="fab fa-product-hunt fa-fw"></i></a></li> -->
                        </ul><!--//social-list-->
                        <?php if (!empty($content)) { ?>
                            <a class="btn btn-primary" href="<?= url('Support/createPDF/' . $content->permission_id) ?>">Download PDF</a>
                        <?php } ?>
                        <!--a href="" class="btn btn-primary d-none d-lg-flex">Download</a-->
                    </div><!--//docs-top-utilities-->
                </div><!--//container-->
            </div><!--//branding-->
        </header><!--//header-->

        <div class="docs-wrapper">
            <div id="docs-sidebar" class="docs-sidebar">
                <div class="top-search-box d-lg-none p-3">
                    <form class="search-form" action="" method="POST">
                        <input type="text" placeholder="Search Here..." name="search" class="form-control search-input">
                        <button type="submit" class="btn search-btn" value="Search"><i class="fas fa-search"></i></button>
                        <?= csrf_field() ?>
                    </form>
                </div>
                <nav id="docs-nav" class="docs-nav navbar">
                    <ul class="section-items list-unstyled nav flex-column pb-3">
                        <li class="nav-item section-title"><a class="nav-link scrollto active" href="#section-1">
                                <span class="theme-icon-holder mr-2"><i class="fas fa-map-signs"></i></span>Start Electronics Payment</a></li>
                        <li class="nav-item" ><a class="nav-link " href="<?= url('Support/page/accounts#system_setting') ?>">System Setting</a></li>
                        <li class="nav-item" ><a class="nav-link " href="<?= url('Support/page/accounts#data_entry') ?>"> Data Entry</a></li>
                        <li class="nav-item" ><a class="nav-link " href="<?= url('Support/page/accounts#setup_of_account') ?>">Setup of Account</a></li>
                        <li class="nav-item" ><a class="nav-link " href="<?= url('Support/page/accounts#invoice') ?>">invoicing</a></li>
                        <li class="nav-item" ><a class="nav-link " href="<?= url('Support/page/accounts#fee_collection') ?>"> Fee Collections</a></li>

                        <?php
                        // echo '<li class="nav-item section-title"><a class="nav-link scrollto active" href="#section-1"><span class="theme-icon-holder mr-2"><i class="fas fa-map-signs"></i></span>' . $start->name . '</a></li>';
                        // $starts = \App\Models\Permission::where('permission_group_id', $start->id)->get();
                        // foreach ($starts as $top) {
                        //     echo '<li class="nav-item"><a class="nav-link" href="' . url('Support/show/' . $top->id) . '"> How to ' . ucfirst(str_replace('_', ' ', $top->name)) . ' </a></li>';
                        // }
                        $lists = DB::table('permission_group')->where('name', '<>', 'Start Electronics Payments')->get();
                        foreach ($lists as $list) {
                            ?>
                            <li class="nav-item section-title">
                                <a class="nav-link scrollto active"data-toggle="collapse" href="#collapse<?=$list->name?>" role="button" aria-expanded="false" aria-controls="collapseExample" href="#section-1"><span class="theme-icon-holder mr-2"><i class="fas fa-map-signs"></i></span><?= $list->name ?></a></li>
                            <?php
                            $list_datas = \App\Models\Permission::where('permission_group_id', $list->id)->get();
                            $perm ='.show';
                            if (!empty($list_datas)) {
                                foreach ($list_datas as $data){
                                    $check = \App\Models\Guide::where('permission_id', $data->id)->first();
                                   
                                    $perm_nam = '';
                                    $show = '';

                                    if (!empty($check)) {
                                        if (!empty($content)){
                                            $perm_nam = $content->permission->permissionGroup->name;
                                            if($perm_nam == $list->name){
                                                $show ='.show';
                                            }
                                            else{
                                                $show ='';
                                            }
                                         }
                                        ?>
                                  <div class="collapse<?=$show?>" id="collapse<?=$list->name?>">
                                        <li class="nav-item" style="background-color: #cccc; border-bottom: 1px solid #fff"><a class="nav-link " href="<?= url('Support/show/' . $data->id) ?>"> How to <?= ucfirst(str_replace('_', ' ', $data->name)) ?> </a></li>
                                    </div>
                                        <?php
                                    }
                                   
                                }
                            }
                        }
                        ?>
                                    <!-- <li class="nav-item section-title mt-3"><a class="nav-link scrollto" href="#section-9"><span class="theme-icon-holder mr-2"><i class="fas fa-lightbulb"></i></span>FAQs</a></li>
                                    <li class="nav-item"><a class="nav-link scrollto" href="#item-9-1">Section Item 9.1</a></li>
                                    <li class="nav-item"><a class="nav-link scrollto" href="#item-9-2">Section Item 9.2</a></li>
                                    <li class="nav-item"><a class="nav-link scrollto" href="#item-9-3">Section Item 9.3</a></li> -->
                    </ul>

                </nav><!--//docs-nav-->
            </div><!--//docs-sidebar-->

            <div class="docs-content">
                <div class="container">

                    <?php if (isset($searches) && !empty($searches)) { ?>
                        <section class="docs-intro">
                            <?php
                            foreach ($searches as $search) {
                                ?>
                                <p><a class="nav-link " href="<?= url('Support/show/' . $search->id) ?>"  style="color: red">How to <?= str_replace('_', ' ', $search->name) ?></a></p>
                            <?php } ?>
                        </section>
                        <!--//docs-intro-->

                        <?php
                    }
                    if (isset($content) && !empty($content)) {
                        ?>

                        <article class="docs-article" id="section-1">
                            <header class="docs-header">
                                <h2 class="docs-heading">How to <?= str_replace('_', ' ', $content->permission->name) ?> <span class="docs-time">Last Modified On: <?= $content->created_at ?> </span></h2>
                            </header>
                            <section class="docs-intro">
                                <p style="color: red"><?= $content->content ?></p>
                            </section><!--//docs-intro-->
                         </article>
                        <footer class="footer">
                            <div class="container text-center py-5">
                                <section style="align-content:center;" class="container-fluid docs-section" id="item-9-2">
                                    <ul>
                                        <p> 
                                            <a class="btn btn-primary" href="#" class="btn btn-primary">Previous  <i class="fa fa-arrow-left"> </i></a>
<!--                                            <a class="btn btn-info" href="<?= url('Support/edit/' . $content->id) ?>"  class="btn btn-primary"> <i class="fa fa-edit"> </i> Edit </a>
                                            <a class="btn btn-danger" href="<?= url('Support/delete/' . $content->id) ?>"  class="btn btn-primary"><i class="fa fa-trash"> </i>  Delete </a>-->
                                            <a class="btn btn-primary" href="#" class="btn btn-primary"> Next  <i class="fa fa-arrow-right"> </i></a></p>
                                    </ul>
                                </section>
                            </div>
                        </footer>
                        <?php } else if(isset($page) && isset ($system_setting->content)) { 
                             echo '<div id="system_setting">'.$system_setting->content.'</div>';
                            echo $system_setting->content;
                            echo '<div id="data_entry">'.$data_entry->content.'</div>';
                            echo $data_entry->content;
                            echo '<div id="setup_of_account">'.$setup_of_account->content.'</div>';
                            echo $setup_of_account->content;
                            echo '<div id="invoice">'.$invoice->content.'</div>';
                            echo $invoice->content;
                            echo '<div id="fee_collection">'.$fee_collection->content.'</div>';
                            echo $fee_collection->content;
                            
                        }else{?>
                            @include('welcome')
                        <?php } ?>
                    </div> 
                </div>
        </div><!--//docs-wrapper-->
    </div> 
</div>
</div><!--//docs-wrapper-->
<!-- Javascript -->          
<script src="<?= $root ?>assets/plugins/jquery-3.4.1.min.js"></script>
<script src="<?= $root ?>assets/plugins/popper.min.js"></script>
<script src="<?= $root ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
<!-- Page Specific JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/highlight.min.js"></script>
<script src="<?= $root ?>assets/js/highlight-custom.js"></script> 
<script src="<?= $root ?>assets/plugins/jquery.scrollTo.min.js"></script>
<script src="<?= $root ?>assets/plugins/lightbox/dist/ekko-lightbox.min.js"></script> 
<script src="<?= $root ?>assets/js/docs.js"></script> 
</body>
</html> 