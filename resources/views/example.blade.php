<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title>ShuleSoft System Documentation</title>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.1/summernote.js"></script>

    <script src="./uploadcare.js"></script>

    <script>
      $(document).ready(function() {
        $('#summernote').summernote({
          height: 460,   //set editable area's height
          placeholder: "Write User Guide on How to <?= ucfirst(str_replace('_', ' ', $guide->name))?> Here...",
          uploadcare: {
            publicKey: 'demopublickey',
            crop: 'free',
            tabs: 'file url instagram',
            tooltipText: 'Upload files or video or something',
            multiple: true
          },
          toolbar: [
            ['uploadcare', ['uploadcare']], // this is the button
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['media', 'link', 'hr', 'picture', 'video']],
            ['view', ['fullscreen', 'codeview']],
            ['help', ['help']]
          ]
        })
      })
    </script>
  </head>
  <body>
  <div class="container">

  <form method="post" action="">
 
<div class="form-group">
   <h1>How to <?= ucfirst(str_replace('_', ' ', $guide->name))?>  </h1>
   <hr>
</div>

<div class="form-group">
<label for="message-text" class="control-label">Content:</label>
<input type="hidden" name="permission_id" value="<?=$guide->id?>">
<textarea id="summernote" name="content" ></textarea>
</div>

<div class="modal-footer">
<a href="<?php echo url('Support/show'); ?>" class="btn btn-default" data-dismiss="modal">Go Back</a>
<button type="submit" class="btn btn-primary" id="add_faq">Submit</button>
</div>
<?= csrf_field() ?>
</form>
</div>

  </body>
</html>
