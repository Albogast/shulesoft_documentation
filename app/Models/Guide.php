<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User Guide
 
 * @package App\Models
 */
class Guide extends Model
{
	protected $table = 'guides';

	protected $casts = [
		'student_id' => 'int',
		'fee_id' => 'int',
		'payment_id' => 'int',
		'amount' => 'float',
		'created_at' => 'timestamp without time zone',
		'updated_at' => 'timestamp without time zone'
	];

	protected $fillable = [
		'content',
		'created_by',
		'permission_id',
		'created_at',
		'updated_at',
		'language'
	];

	public function permission()
	{
		return $this->belongsTo(Permission::class, 'permission_id',  'id');
	}

}
