<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @package App\Models
 */
class Faq extends Model
{
	protected $table = 'faq';

	protected $fillable = [
		'question',
		'answer',
		'permission_id',
		'created_at',
		'updated_at'
	];

	public function permission()
	{
		return $this->belongsTo(Permission::class, 'permission_id', 'id');
	}
}
