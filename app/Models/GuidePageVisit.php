<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdvancePayment
 * 
 * @property int $id
 * @property int|null $student_id
 * @property int|null $fee_id
 * @property int|null $payment_id
 * @property float|null $amount
 * @property timestamp without time zone $created_at
 * @property timestamp without time zone|null $updated_at
 * 
 * @property Student $student
 * @property Fee $fee
 * @property Payment $payment
 * @property Collection|InvoicesFeesInstallment[] $invoices_fees_installments
 *
 * @package App\Models
 */
class GuidePageVisit extends Model
{
	protected $table = 'guide_page_visits';

	protected $casts = [
		'student_id' => 'int',
		'fee_id' => 'int',
		'payment_id' => 'int',
		'amount' => 'float',
		'created_at' => 'timestamp without time zone',
		'updated_at' => 'timestamp without time zone'
	];

	protected $fillable = [
		'student_id',
		'fee_id',
		'payment_id',
		'amount'
	];

	public function student()
	{
		return $this->belongsTo(Student::class)
					->where('student.student_id', '=', 'advance_payments.student_id')
					->where('student.student_id', '=', 'advance_payments.student_id')
					->where('student.student_id', '=', 'advance_payments.student_id')
					->where('student.student_id', '=', 'advance_payments.student_id');
	}

	public function fee()
	{
		return $this->belongsTo(Fee::class)
					->where('fees.id', '=', 'advance_payments.fee_id')
					->where('fees.id', '=', 'advance_payments.fee_id')
					->where('fees.id', '=', 'advance_payments.fee_id')
					->where('fees.id', '=', 'advance_payments.fee_id');
	}

	public function payment()
	{
		return $this->belongsTo(Payment::class)
					->where('payments.id', '=', 'advance_payments.payment_id')
					->where('payments.id', '=', 'advance_payments.payment_id')
					->where('payments.id', '=', 'advance_payments.payment_id')
					->where('payments.id', '=', 'advance_payments.payment_id');
	}

	public function invoices_fees_installments()
	{
		return $this->belongsToMany(InvoicesFeesInstallment::class, 'advance_payments_invoices_fees_installments', 'advance_payment_id', 'invoices_fees_installments_id')
					->withPivot('id', 'amount')
					->withTimestamps();
	}
}
