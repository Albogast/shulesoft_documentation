<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


class Permission extends Model
{
	protected $table = 'permission';

	protected $casts = [
		'name' => 'character varying',
		'created_at' => 'timestamp without time zone',
		'updated_at' => 'timestamp without time zone',
		'category_id' => 'int',
		'financial_category_id' => 'int',
		'predefined' => 'int'
	];

	protected $fillable = [
		'name',
		'display_name',
		'description',
		'is_super',
		'permission_group_id',
		'created_at',
		'arrangement'
	];

	public function permissionGroup()
	{
		return $this->belongsTo(PermissionGroup::class, 'permission_group_id', 'id');
	}

}
