<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AdvancePaymentsInvoicesFeesInstallment
 * 
 * @property int $id
 * @property int|null $invoices_fees_installments_id
 * @property int|null $advance_payment_id
 * @property float|null $amount
 * @property timestamp without time zone|null $created_at
 * @property timestamp without time zone|null $updated_at
 * 
 * @property InvoicesFeesInstallment $invoices_fees_installment
 * @property AdvancePayment $advance_payment
 *
 * @package App\Models
 */
class PermissionGroup extends Model
{
	protected $table = 'permission_group';


	protected $fillable = [
		'name',
		'created_at',
		'module_id'
	];

	public function permission()
	{
		return $this->hasMany(Permission::class, 'id', 'permission_group_id');
	}
	
}
