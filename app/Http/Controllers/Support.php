<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Guide;
use Illuminate\Http\Request;
use App\Models\Permission;
use DB;
use PDF;

class Support extends Controller {

    /**
     * 
     * PUT A DISCLAIMER, THAT these websites are still under development and in case you face any challenge, kindly help us to improve
     * These are remaining parts
     * 
     * 1. loading page based on user permissions. The system will first request user to login/ or take session from users
     * who opens from ShuleSoft and then show pages based on permissions
     * 
     * 2. disable options to edit/delete and put conditions only with specific user
     * 
     * 3. put an option to track users who visit each page and track every click
     * 
     * 4. track all search queries 
     * 
     * 5. put an option for user to comment if the page is useful or not with option to add a comment which will only be seen by ShuleSoft team in admin panel
     * 
     * 6. put an option to share the page via whatsapp, linkedin, twitter and facebook
     *  
     * 7. download button should appear stratergically to cover a certain topic. (grouped download button that when user click , ask to download these contents (getting started,
     * account training begineer, account intermediate, exams, etc based on syllabus given). Page needs to be combined, and not writting something new
     * 
     * 8 optimize manual for parents only, students only and teachers only
     * 
     */
    public function __construct() {
        // $this->middleware('auth');
    }

    public function index() {
        $this->data['home'] = 'Home';
        return view('index', $this->data);
    }

    public function show() {
        if ($_POST) {
            $q = request('search');
            if ($q != "") {
                $permission = Permission::where('name', 'LIKE', '%' . $q . '%')->get();
                if (count($permission) > 0) {
                    $this->data['searches'] = $permission;
                }
            }
        }
        $id = request()->segment(3);
        if ((int) $id > 0) {
            $this->data['content'] = \App\Models\Guide::where('permission_id', $id)->first();
        } else if (strlen($id) > 4) {
            $this->data['content'] = \App\Models\Guide::whereIn('permission_id', \App\Models\Permission::where('name', $id)->get(['id']))->first();
        } else {
            $this->data['content'] = [];
        }
        $last_update = \App\Models\Guide::orderBy('created_at', 'desc')->first();
        $this->data['last_update'] = !empty($last_update) ? $last_update->created_at : '';
        $this->data['start'] = \App\Models\PermissionGroup::first();
        return view('document', $this->data);
    }

    public function page() {
          $last_update = \App\Models\Guide::orderBy('created_at', 'desc')->first();
        $this->data['last_update'] = !empty($last_update) ? $last_update->created_at : '';
        $this->data['start'] = \App\Models\PermissionGroup::first();
        $this->data['page'] = 1;
        $id = request()->segment(3);
        switch ($id) {
            case 'accounts':
                $this->data['system_setting'] = \App\Models\Guide::whereIn('permission_id', \App\Models\Permission::where('name', 'system_setting')->get(['id']))->first();
                $this->data['data_entry'] = \App\Models\Guide::whereIn('permission_id', \App\Models\Permission::where('name', 'data_entry')->get(['id']))->first();
                $this->data['setup_of_account'] = \App\Models\Guide::whereIn('permission_id', \App\Models\Permission::where('name', 'setup_of_account')->get(['id']))->first();
                $this->data['invoice'] = \App\Models\Guide::whereIn('permission_id', \App\Models\Permission::where('name', 'invoice')->get(['id']))->first();
                $this->data['fee_collection'] = \App\Models\Guide::whereIn('permission_id', \App\Models\Permission::where('name', 'fee_collection')->get(['id']))->first();

                return view('document', $this->data);
                break;

            default:
                break;
        }
    }

    // //Generate PDF
    public function createPDF() {
        // retreive all records from db
        // $data = Guide::all();

        $id = request()->segment(3);
        if ((int) $id > 0) {
            $this->data['content'] = \App\Models\Guide::where('permission_id', $id)->first();
        } else {
            $this->data['content'] = [];
        }
        // $this->data['start'] = \App\Models\PermissionGroup::where('name', 'Getting Started')->first();
        // share data to view
        view()->share('pdf_view', $this->data);
        $pdf = PDF::loadView('pdf_view', $this->data);

        // download PDF file with download method
        return $pdf->download('How to ' . $this->data['content']->permission->name . '.pdf');
    }

    public function add() {
        $id = request()->segment(3);
        if ($_POST) {
            $data = request()->all();
            $create = \App\Models\Guide::create($data);
            return redirect('Support/show/' . request('permission_id'))->with('success', 'Data Added successfully');
        }
        $this->data['guide'] = \App\Models\Permission::where('id', $id)->first();
        return view('example', $this->data);
    }

    public function edit() {
        $id = request()->segment(3);

        if ($_POST) {
            $data = request()->except('files', '_token');
            \App\Models\Guide::where('id', $id)->update($data);
            return redirect('Support/show/' . request('permission_id'))->with('success', 'Data Updated successfully');
        }
        $this->data['guide'] = \App\Models\Guide::where('id', $id)->first();
        $this->data['permissions'] = \App\Models\Permission::orderBy('name', 'DESC')->get();

        return view('edit', $this->data);
    }

    public function delete() {
        $id = request()->segment(3);
        $this->data['guide'] = \App\Models\Guide::where('id', $id)->delete();
        return redirect('Support/show/')->with('success', 'Data Deleted successfully');
    }

}
